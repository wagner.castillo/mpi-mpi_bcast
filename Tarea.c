#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int obtenerCount(int i, int a, int b, int n);
float obtenerResultado(int lim, int count);

int main(void) {

    int i=0, a=0, b=0;
    int limite, Count=0;
    int rank, size;
    float Salida=0;
    
    /* 
        int MPI_Init (int *argc, char ***argv)
        argc	Puntero al número de argumentos
        argv	Puntero al vector de argumentos
    
    */

    MPI_Init(NULL, NULL);                 // Inicializa la estructura de comunicación de MPI entre los procesos.
    MPI_Comm_size(MPI_COMM_WORLD, &size); // Obtener el tamaño del comunicador selecionado
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); // Obtener el rango del proceso que lo llama en el grupo de comunicación.

    if (rank == 0) {  limite = 1000000; }

      /*
        int MPI_Bcast(void* buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm)
        Envía un único mensaje desde el proceso que tenga el rango de raíz (root) a todos los procesos del grupo
      */
    
        MPI_Bcast(
        /* Buffer    */  &limite,
        /* Count     */  1,
        /* Datatype  */  MPI_INT, 
        /* root      */  0, 
        /* MPi_Comm  */  MPI_COMM_WORLD );

    Count = obtenerCount(i, a, b, limite);
    Salida = obtenerResultado(limite, Count);

    if (rank == 0) { printf("La Respuesta es: %f\n", Salida); }

    MPI_Finalize();
    return 0;

}


// Calculos

float obtenerResultado(int lim, int count) {
    float res = (4 * ((float)count / lim));
    return res;
}

int obtenerCount(int i, int a, int b, int n)
{
    int count = 0;
    while (i < n)
    {
        a = rand();
        b = rand();
        if ((a * a) + (b * b) < 1)
        {
            count++;
        }
        i++;
    }
    return count;
}

